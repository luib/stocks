FROM node:7.8.0

ENV NPM_CONFIG_LOGLEVEL warn

COPY package.json package.json
RUN npm install

RUN npm install -g serve

COPY . .

RUN npm run build

EXPOSE 5000

CMD ["serve", "-s", "build"]