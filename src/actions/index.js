import {REMOVE, UPDATE, TOGGLE_VIEW, PRICE_FETCH_REQUEST, ADD_STOCK_REQUEST, PRICE_FETCH_REQUEST_ALL, HIDE_MODAL, TOGGLE_MODAL, LOGIN_REQUEST, LOGIN_REQUEST_OK, LOGIN_REQUEST_KO, LOGOUT_REQUEST} from '../constants';

export const AddStock = (symbol, quantity) => {
    return {
        type: ADD_STOCK_REQUEST,
        symbol,
        quantity,
    };
}

export const RemoveStock = (symbol) => {
    return {
        type: REMOVE,
        symbol,
    };
}

export const UpdateStock = (symbol, quantity) => {
    return {
        type: UPDATE,
        symbol,
        quantity,
    };
}

export const toggleViewMode = () => {
    return {
      type: TOGGLE_VIEW,
    };
}

export const FetchStock = (symbol) => {
    return {
      type: PRICE_FETCH_REQUEST,
      symbol,
    };
}

export const FetchStockAll = (symbol) => {
    return {
      type: PRICE_FETCH_REQUEST_ALL,
    };
}

export const hideModal = () => {
    return {
        type: HIDE_MODAL,
    };
}

export const toggleModal = (modalType) => {
    return {
        type: TOGGLE_MODAL,
        currentModal: modalType,
    };
}

export const Login = (username, password) => {
    return {
        type: LOGIN_REQUEST,
        username,
        password,
    };
}

export const LoginOK = (username, password) => {
    return {
        type: LOGIN_REQUEST_OK,
        username,
        password,
    };
}

export const LoginKO = (username, password) => {
    return {
        type: LOGIN_REQUEST_KO,
        username,
        password,
    };
}

export const Logout = () => {
    return {
        type: LOGOUT_REQUEST,
    };
}