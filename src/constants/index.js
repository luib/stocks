// api urls
export const API_URL = 'https://api.iextrading.com/1.0/stock/'

// action type constants
export const TOGGLE_VIEW = 'toggle_view'
export const ADD_STOCK_REQUEST_SUCCESS = 'add_stock_request_success'
export const REMOVE = 'remove'
export const UPDATE = 'update'
export const LOGIN_REQUEST = 'login_request'
export const LOGOUT_REQUEST = 'logout_request'

// view mode constants
export const LAST_PRICE = 'last_price'
export const EQUITY = 'equity'
export const PERCENT_CHANGE = 'percent_change'

export const LAST_PRICE_FRIENDLY = 'Last Price'
export const EQUITY_FRIENDLY = 'Equity'
export const PERCENT_CHANGE_FRIENDLY = 'Percent Change'

// saga action types
export const ADD_STOCK_REQUEST = 'add_stock_request'
export const PRICE_FETCH_REQUEST = 'price_fetch_request'
export const PRICE_FETCH_REQUEST_ALL = 'price_fetch_request_all'
export const PRICE_FETCH_REQUEST_OK = 'price_fetch_request_ok'
export const PRICE_FETCH_REQUEST_KO = 'price_fetch_request_ko'
export const LOGIN_REQUEST_OK = 'login_request_ok'
export const LOGIN_REQUEST_KO = 'login_request_ko'

// modal constants
export const TOGGLE_MODAL = 'toggle_modal';
export const HIDE_MODAL = 'hide_modal';
export const MOTD_TYPE_MODAL = 'motd_type_modal';