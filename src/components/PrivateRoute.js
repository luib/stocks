import React from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import {
    Route,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions';
import { getLoginMemoized } from '../selectors/index';

const PrivateRoute = ({ loginState, path, component: Component, actions, ...others }) => {
    const getDestinationRender = (props) => {
        if (loginState.loggedIn) {
            return <Component {...props} />;
        } else {
            return (
                <Redirect to={{
                    pathname: '/login',
                    state: { from: props.location }
                }} />);
        }
    }

    return (
        <Route path={path} {...others} render={getDestinationRender} />
    );
};

const mapStateToProps = createSelector(getLoginMemoized(), (loginState) => ({ loginState }));

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Actions, dispatch),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PrivateRoute));