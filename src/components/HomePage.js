import React from 'react';

const HomePage = (props) => {
    return (
        <div>
            <header> Welcome to the Stocks app.</header>
            <p>Use credentials: <b>admin</b>:<b>password</b></p>
        </div>
    );
};

export default HomePage