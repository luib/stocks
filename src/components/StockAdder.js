import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import { bindActionCreators } from 'redux';
import { Button, FormGroup, ControlLabel, FormControl, Alert } from 'react-bootstrap';
import { getStocksMemoized } from '../selectors';
import { createSelector } from 'reselect';

class StockAdder extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
            quantity: '',
            submitted: '',
        };
    }

    handleChange = (e) => {
        this.setState({
            value: e.target.value.toUpperCase(),
        });
    }

    handleQuantityChange = (e) => {
        this.setState({
            quantity: e.target.value,
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.actions.AddStock(this.state.value, parseInt(this.state.quantity, 10));
        this.setState((state) => {
            return {
                submitted: state.value,
            }
        });
    }

    getValidationState = () => {
        const length = this.state.value.length;
        if (length > 0) return 'success';
        else return 'error';
    }

    getValidationStateQuantity = () => {
        const quantity = this.state.quantity;
        if (!isNaN(quantity) && parseInt(quantity, 10) > 0) return 'success';
        else return 'error';
    }

    renderStatus = () => {
        if (!this.state.submitted || this.props.allStocks.success === undefined) {
            return null;
        }
        return <Alert bsStyle={this.props.allStocks.success ? 'success' : 'danger'}>{this.props.allStocks.success ? 'Found ' + this.state.submitted : 'Did not find ' + this.state.submitted}</Alert>
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                {this.renderStatus()}
                <FormGroup
                    controlId="formBasicText"
                    validationState={this.getValidationState()}
                >
                    <ControlLabel>Stock</ControlLabel>
                    <FormControl
                        type="text"
                        value={this.state.value}
                        placeholder="AAPL"
                        onChange={this.handleChange}
                    />
                    <FormControl.Feedback />

                </FormGroup>

                <FormGroup
                    controlId="formBasicNumber"
                    validationState={this.getValidationStateQuantity()}
                >
                    <ControlLabel>Quantity</ControlLabel>
                    <FormControl
                        type="number"
                        value={this.state.quantity}
                        placeholder="1"
                        onChange={this.handleQuantityChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <Button type="submit" disabled={this.getValidationState() !== 'success' || this.getValidationStateQuantity() !== 'success'}>
                    Submit
                </Button>
            </form>
        );
    }
}

const mapStateToProps = createSelector(getStocksMemoized(), (allStocks)=>({allStocks}));

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Actions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(StockAdder);
