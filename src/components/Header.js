import React from 'react';
import {Navbar, Nav, NavItem, NavDropdown, MenuItem, FormGroup, FormControl, Button} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions';
import { getLoginMemoized } from '../selectors/index';

const Header = (props) => {
  const renderLoginLogoutButton = () => {
    if (!props.loginState.loggedIn) {
      return (
        <LinkContainer to="/login"><MenuItem eventKey={3.4}>Login</MenuItem></LinkContainer>
      );
    } else {
      return (
        <LinkContainer to="/logout"><MenuItem eventKey={3.4}>Logout</MenuItem></LinkContainer>
      );
    }
  };

  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/">Stocks</Link>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav>
        <LinkContainer to="/view"><NavItem eventKey={1}>My Stocks</NavItem></LinkContainer>
        <LinkContainer to="/add"><NavItem eventKey={2}>Buy</NavItem></LinkContainer>
        <NavDropdown eventKey={3} title="My Account" id="basic-nav-dropdown">
          <MenuItem eventKey={3.1}>Overview</MenuItem>
          <MenuItem eventKey={3.2}>History</MenuItem>
          <MenuItem eventKey={3.3}>Preferences</MenuItem>
          <MenuItem divider />
          {renderLoginLogoutButton()}
        </NavDropdown>
      </Nav>
      <Navbar.Form pullLeft>
        <FormGroup>
          <FormControl type="text" placeholder="Search" />
        </FormGroup>
        {' '}
        <Button type="submit">Submit</Button>
      </Navbar.Form>
    </Navbar>
  );
};

const mapStateToProps = createSelector(getLoginMemoized(), (loginState) => ({ loginState }));

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Actions, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);