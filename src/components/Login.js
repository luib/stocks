import React, { Component } from 'react';
import { Button, FormGroup, ControlLabel, FormControl, Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions';
import { getLoginMemoized } from '../selectors/index';
import { Redirect } from 'react-router-dom';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            submitted: false,
        };

        const { from } = this.props.location.state || { from: { pathname: '/' } }

        this.from = from;
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.actions.Login(this.state.username, this.state.password);

        this.setState({
            submitted: true,
        });
    }

    getValidationStateUsername = () => {
        const length = this.state.username.length;
        if (length > 0) return 'success';
        else return 'error';
    }

    getValidationStatePassword = () => {
        const length = this.state.password.length;
        if (length > 0) return 'success';
        else return 'error';
    }

    handleUsernameChange = (e) => {
        this.setState({
            username: e.target.value,
        });
    }

    handlePasswordChange = (e) => {
        this.setState({
            password: e.target.value,
        });
    }

    renderInfoMsg = () => {
        if (this.state.submitted && this.props.loginState.error) {
            return (
                <Alert bsStyle="danger">
                    Login failed.
                </Alert>
            );
        }

        if (this.from.pathname !== '/') {
            return (
                <Alert bsStyle="warning">
                    You must login to access this feature.
                </Alert>
            );
        }
    }

    render() {
        if (this.props.loginState.loggedIn) {
            return <Redirect to={this.from} />;
        }

        return (
            <div>
                {this.renderInfoMsg()}
                <form onSubmit={this.handleSubmit}>
                    <FormGroup
                        controlId="formBasicText"
                        validationState={this.getValidationStateUsername()}
                    >
                        <ControlLabel>Username</ControlLabel>
                        <FormControl
                            type="text"
                            value={this.state.username}
                            onChange={this.handleUsernameChange}
                        />
                        <FormControl.Feedback />

                    </FormGroup>

                    <FormGroup
                        controlId="formBasicNumber"
                        validationState={this.getValidationStatePassword()}
                    >
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            type="password"
                            value={this.state.password}
                            onChange={this.handlePasswordChange}
                        />
                        <FormControl.Feedback />
                    </FormGroup>
                    <Button type="submit" disabled={this.getValidationStateUsername() !== 'success' || this.getValidationStatePassword() !== 'success'}>
                        Submit
                    </Button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = createSelector(getLoginMemoized(), (loginState) => ({ loginState }));

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Actions, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);