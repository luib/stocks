import React from 'react';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import { bindActionCreators } from 'redux';
import StockItem from './StockItem';
import { ListGroup } from 'react-bootstrap';
import { getViewModeFriendly, getStocksMemoized } from '../selectors';
import './StockList.css';
import { createStructuredSelector } from 'reselect';

const StockList = (props) => {

    const renderStocks = () => {
        if (Object.keys(props.allStocks.stocks).length === 0) {
            return <h1> No stocks added.</h1>;
        }

        return Object.keys(props.allStocks.stocks).map(s => {
            return <StockItem key={s} symbol={s} data={props.allStocks.stocks[s]} onDelete={handleDeleteStock} />
        });
    }

    const renderColumns = () => {
        if (Object.keys(props.allStocks.stocks).length === 0) {
            return null;
        }

        return (
            <div className="stock-list-header">
                <h3 className="stock-column-header">
                    Stock
                </h3>
                <h3 className="view-mode-column-header">
                    {props.view}
                </h3>
            </div>
        );
    }

    const renderRefresh = () => {
        if (Object.keys(props.allStocks.stocks).length === 0) {
            return null;
        }

        return (
            <button onClick={handleRefresh}>Refresh</button>
        );
    }

    const renderLastUpdated = () => {
        if (!props.allStocks.lastUpdated || Object.keys(props.allStocks.stocks).length === 0) {
            return null;
        }
        
        return (
            <div>Last updated at {props.allStocks.lastUpdated.toString()}</div>
        );
    }

    const handleRefresh = () => {
        props.actions.FetchStockAll();
    }

    const handleDeleteStock = (symbol) => {
        props.actions.RemoveStock(symbol);
    }

    return (
        <div>
            <ListGroup>
                {renderColumns()}
                {renderStocks()}
            </ListGroup>
            {renderRefresh()}
            {renderLastUpdated()}
        </div>
    );

}

const mapStateToProps = createStructuredSelector({
    view: getViewModeFriendly(),
    allStocks: getStocksMemoized(),
});

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StockList);
