import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions';
import { getLoginMemoized } from '../selectors/index';

class Logout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 3
        };
    }

    componentDidMount() {
        if (this.props.loginState.loggedIn) {
            this.logoutTimer = setInterval(() => {
                if (this.state.time <= 0) {
                    clearInterval(this.logoutTimer);

                    this.props.actions.Logout();
                    this.props.history.push('/');
                } else {
                    this.setState((state) => {
                        return {
                            time: state.time - 1
                        };
                    })
                }

            }, 1000);
        } else {
            this.props.history.push('/login');
        }
    }

    componentWillUnmount() {
        clearInterval(this.logoutTimer);
    }

    renderMessage = () => {
        return (
            <p>Logout in {this.state.time} seconds ... </p>
        )
    }

    render() {
        return (
            <div>
                {this.renderMessage()}
            </div>
        );
    }
}

const mapStateToProps = createSelector(getLoginMemoized(), (loginState) => ({ loginState }));

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Actions, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);