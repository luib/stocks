import React from 'react';
import PropTypes from 'prop-types';
import { LAST_PRICE, EQUITY, PERCENT_CHANGE } from '../constants';
import { connect } from 'react-redux';
import * as Actions from '../actions';
import { bindActionCreators } from 'redux';
import './StockValue.css';
import { getViewMode } from '../selectors';
import { createSelector } from 'reselect';

const StockValue = ({ data, view, actions }) => {

  const handleClick = (e) => {
    e.preventDefault();

    actions.toggleViewMode();
  }

  const getDisplayPrice = () => {
    if (!data.price) {
      return 'N/A';
    } else if (view === LAST_PRICE) {
      return '$' + data.price.toFixed(2);
    } else if (view === EQUITY) {
      return '$' + (data.quantity * data.price).toFixed(2);
    } else if (view === PERCENT_CHANGE) {
      if (data.changePercent > 0) {
        return '+' + data.changePercent.toFixed(2) + '%';
      } else {
        return data.changePercent.toFixed(2) + '%';
      }
    }
  }

  const getCSSClass = () => {
    if (!data.changePercent || data.changePercent === 0) {
      return 'stock-value stock-value--neutral';
    } else if (data.changePercent > 0) {
      return 'stock-value stock-value--positive';
    } else {
      return 'stock-value stock-value--negative';
    }
  }

  return (
    <span className={getCSSClass()} onClick={(e) => handleClick(e)}>
      {getDisplayPrice()}
    </span>
  );
};

const mapStateToProps = createSelector(getViewMode(), (view) => ({view}));

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
};

StockValue.propTypes = {
  data: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(StockValue);
