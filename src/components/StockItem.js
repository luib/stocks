import React from 'react';
import {ListGroupItem} from 'react-bootstrap';
import PropTypes from 'prop-types';
import './StockItem.css';
import StockValue from './StockValue';

const StockItem = ({symbol, data, onDelete}) => {
  function renderQuantityText() {
    return data.quantity + (data.quantity > 1 ? ' shares' : ' share');
  }

  function handleDelete(e) {
    e.preventDefault();
    onDelete(symbol);
  }
    return (
        <ListGroupItem header={symbol} href="#">
          <span className="stock-quantity">{renderQuantityText()}</span>
          <span className="stock-price">
            <StockValue data={data}/>
          </span>
          <span className="stock-remove-btn" onClick={handleDelete}>X</span>

        </ListGroupItem>
    );
};

StockItem.propTypes = {
    data: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
};

export default StockItem;
