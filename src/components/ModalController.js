import React, { Component } from 'react';
import * as Actions from '../actions/';
import { bindActionCreators } from 'redux';
import { getModalMemoized } from '../selectors';
import { MOTD_TYPE_MODAL } from '../constants';
import MOTDModal from './MOTDModal';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';

class ModalController extends Component {
    renderModal = () => {
        switch(this.props.modal.currentModal) {
            case MOTD_TYPE_MODAL : {
                return <MOTDModal onClose={this.props.actions.hideModal} />;
            }
            default: {
                return null;
            }
        }
    }

    render() {
        return this.renderModal();
    }
}
const mapStateToProps = createSelector(getModalMemoized(), (modal) => ({modal}));

const mapDispatchToProps = (dispatch) => {
  return {
      actions: bindActionCreators(Actions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalController);