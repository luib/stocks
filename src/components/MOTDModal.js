import React, { Component } from 'react';
import Modal from './Modal';

class MOTDModal extends Component {
    render() {
        return (
            <Modal show={true} onClose={this.props.onClose}>
                <h2>Message of the day</h2>
                <p>Welcome to Stocks!</p>
            </Modal>
        );
    }
}

export default MOTDModal;