import {createSelector} from 'reselect';
import * as Constants from '../constants';

const getView = (state) => state.viewReducer;
const getStock = (state) => state.stockReducer;
const getModal = (state) => state.modalReducer;
const getLogin = (state) => state.loginReducer;

export const getViewMode = () => createSelector(getView, a => a);

export const getStocksMemoized = () => createSelector(getStock, a => a);

export const getViewModeFriendly = () => createSelector(getView, a => {
    switch (a) {
        case Constants.LAST_PRICE:
            return Constants.LAST_PRICE_FRIENDLY;
        case Constants.PERCENT_CHANGE:
            return Constants.PERCENT_CHANGE_FRIENDLY;
        case Constants.EQUITY:
            return Constants.EQUITY_FRIENDLY;
        default:
            return 'Unknown';
    }
});

export const getModalMemoized = () => createSelector(getModal, a => a);

export const getLoginMemoized = () => createSelector(getLogin, a => a);