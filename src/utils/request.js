import axios from 'axios';
import {API_URL} from '../constants/index';

export const getStockPrice = (symbol) => {
    console.log('Getting data for ... ' + symbol);
    return axios.get(API_URL + symbol + '/quote');
};