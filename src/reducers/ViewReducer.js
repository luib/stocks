import {TOGGLE_VIEW, LAST_PRICE, EQUITY, PERCENT_CHANGE} from '../constants';

function *generator() {
  while(true) {
    yield EQUITY;
    yield PERCENT_CHANGE;
    yield LAST_PRICE;
  }
}

var it = generator();

const ViewReducer = (state=LAST_PRICE, action) => {
  switch(action.type) {
    case TOGGLE_VIEW: {
      return it.next().value;
    }
    default : {
      return state;
    }
  }
};

export default ViewReducer
