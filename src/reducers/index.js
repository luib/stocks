import {combineReducers} from 'redux';
import stockReducer from './StockReducer';
import viewReducer from './ViewReducer';
import modalReducer from './ModalReducer';
import loginReducer from './LoginReducer';

const index = combineReducers({
    stockReducer,
    viewReducer,
    modalReducer,
    loginReducer,
});

export default index;
