import { TOGGLE_MODAL, HIDE_MODAL, MOTD_TYPE_MODAL } from '../constants';

/*
    state: {
        currentModal: null,
    }
*/
const ModalReducer = (state = { currentModal: MOTD_TYPE_MODAL }, action) => {
    switch (action.type) {
        case TOGGLE_MODAL: {
            return { ...state, currentModal: action.modal };
        }
        case HIDE_MODAL: {
            return { ...state, currentModal: null };
        }
        default: {
            return state;
        }
    }
};

export default ModalReducer
