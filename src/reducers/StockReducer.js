/*
state : {
    stocks:{
        AAPL: {quantity: 1, price: 150, changePercent: 0.10},
        ...
    },
    success: <boolean>,
    lastUpdated: <dateTime>,
}
*/
import {ADD_STOCK_REQUEST_SUCCESS, REMOVE, UPDATE, PRICE_FETCH_REQUEST_OK, PRICE_FETCH_REQUEST_KO} from '../constants';

const StockReducer = (state={stocks:{}}, action) => {
    switch (action.type) {
        case ADD_STOCK_REQUEST_SUCCESS: {
            let copy = {...state};
            if (copy.stocks[action.symbol]) {
                copy.stocks[action.symbol].quantity += action.quantity;
            } else {
                copy.stocks[action.symbol] = {quantity: action.quantity};
            }

            return copy;
        }
        case REMOVE: {
            let copy = {...state};
            delete copy.stocks[action.symbol];
            return copy;
        }
        case UPDATE: {
            let copy = {...state};
            copy.stocks[action.symbol].quantity = action.quantity;
            return copy;
        }
        case PRICE_FETCH_REQUEST_OK: {
            let copy = {...state};
            copy.stocks[action.symbol].price = action.price;
            copy.stocks[action.symbol].changePercent = action.changePercent;
            copy.success = true;
            copy.lastUpdated = new Date();
            return copy;
        }
        case PRICE_FETCH_REQUEST_KO: {
            return {...state, success: false};
        }
        default: {
            return state;
        }
    }
};

export default StockReducer;
