import React, { Component } from 'react';
import './App.css';
import StockList from './components/StockList';
import StockAdder from './components/StockAdder';
import Header from './components/Header';
import ModalController from './components/ModalController';
import HomePage from './components/HomePage';

import {
  Route,
} from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import Login from './components/Login';
import Logout from './components/Logout';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route component={Header} />
        <div className="App-intro">
          <Route exact path="/" component={HomePage} />
          <PrivateRoute exact path="/view" component={StockList} />
          <PrivateRoute exact path="/add" component={StockAdder} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/logout" component={Logout} />
        </div>
        <ModalController />
        <footer>Created by Brian Lui 2017</footer>
      </div>
    );
  }
}

export default App
