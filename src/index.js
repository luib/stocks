import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './configureStore';
import {Provider} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

let store = configureStore();

ReactDOM.render(<Router><Provider store={store}>
    <Route component={App} />
</Provider></Router>, document.getElementById('root'));
registerServiceWorker();
