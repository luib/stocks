import reducers from './reducers';
import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga'
import mySaga from './sagas'


const ConfigureStore = () => {
    // create the saga middleware
    const sagaMiddleware = createSagaMiddleware()
    
    // enable redux devtools extension support
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    // get local redux state if it exists
    const persistedState = localStorage.getItem('reduxState') ? JSON.parse(localStorage.getItem('reduxState')) : {};
    
    // don't store modal state TODO: Change up!
    delete persistedState.modalReducer;
    
    let store = createStore(reducers, persistedState, composeEnhancers(applyMiddleware(sagaMiddleware)));
    
    // store redux state into local storage
    store.subscribe(()=>{
        localStorage.setItem('reduxState', JSON.stringify(store.getState()))
    })

    // then run the saga
    sagaMiddleware.run(mySaga)

    return store;
};


export default ConfigureStore;