import { all, call, put, takeEvery, select, throttle, takeLatest } from 'redux-saga/effects'
import { getStockPrice } from '../utils/request';
import checkLogin from '../utils/loginService';
import * as Constants from '../constants/';
import {getStocksMemoized} from '../selectors/';
import { LoginOK, LoginKO } from '../actions/index';

function* fetchStock(action) {
  try {
    const result = yield call(getStockPrice, action.symbol);
    yield put({
      type: Constants.PRICE_FETCH_REQUEST_OK,
      symbol: action.symbol,
      price: result.data.latestPrice,
      changePercent: result.data.changePercent,
    });
  } catch (e) {
    yield put({ type: Constants.PRICE_FETCH_REQUEST_KO, message: e.message });
  }
}

function* fetchStockAll() {
  const stocks = getStocksMemoized();
  const stockState = yield select(stocks);
  yield all(Object.keys(stockState.stocks).map(s => call(fetchStock, {symbol: s})));
}

function* addStock(action) {
  try {
    // determine if stock symbol is valid
    yield call(getStockPrice, action.symbol);

    yield put({
      type: Constants.ADD_STOCK_REQUEST_SUCCESS,
      symbol: action.symbol,
      quantity: action.quantity,
    });
    yield call(fetchStockAll);
  } catch (e) {
    yield put({ type: Constants.PRICE_FETCH_REQUEST_KO, message: e.message });
  }
}

function* handleLogin({username, password}) {
  try {
    yield call(checkLogin, username, password);

    yield put(LoginOK(username,password));
  } catch (e) {

    yield put(LoginKO(username,password));
  }
}

function* mySaga() {
  yield all([
    takeEvery(Constants.PRICE_FETCH_REQUEST, fetchStock),
    throttle(1000, Constants.PRICE_FETCH_REQUEST_ALL, fetchStockAll),
    takeEvery(Constants.ADD_STOCK_REQUEST, addStock),
    takeLatest(Constants.LOGIN_REQUEST, handleLogin)
  ]);
}

export default mySaga
export {fetchStock}