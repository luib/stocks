import { call, put } from 'redux-saga/effects'
import {fetchStock} from './index';
import {getStockPrice} from '../utils/request';
import * as Constants from '../constants/';

it('should yield the right side effect', () => {
    const iterator = fetchStock({symbol: 'AAPL'});
    
    let actualEffect = iterator.next().value;

    expect(actualEffect).toEqual(call(getStockPrice, 'AAPL'));

    actualEffect = iterator.next({data: {latestPrice: 150, changePercent: 0.01}}).value;

    expect(actualEffect).toEqual(
        put({
            type: Constants.PRICE_FETCH_REQUEST_OK,
            symbol: 'AAPL',
            price: 150,
            changePercent: 0.01,
        })
    );
});

it('should handle error side effects', () => {
    const iterator = fetchStock({symbol: 'AAPL'});

    let actualEffect = iterator.next().value;
    
    expect(actualEffect).toEqual(call(getStockPrice, 'AAPL'));

    const error = {message: 'no stock found'};
    
    actualEffect = iterator.throw(error).value;

    expect(actualEffect).toEqual(
        put({
            type: Constants.PRICE_FETCH_REQUEST_KO, message: error.message,
        })
    );
});
